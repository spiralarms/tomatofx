package org.spiralarms.tomatofx;


public interface IView {
	
	// View asks for information and sends user input to controller
	void setController(IController controller);
	
	// Very important method
	void update();

	// Less important methods
	// They will be dropped in future
	//void drawPauseOnPlay();
	//void drawStartOnPlay();

}
