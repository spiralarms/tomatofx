package org.spiralarms.tomatofx;

import javafx.scene.media.AudioClip;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Logger;

/**
 * Sound system for the application. Concurrently plays short sounds using
 * String id
 */
public class SoundManager {
	private Logger log = Logger.getLogger(getClass().getName());

	private ExecutorService soundPool = Executors.newFixedThreadPool(2);
	private Map<String, AudioClip> soundMap = new HashMap<>();

	/**
	 * Create a simple thread pool.
	 *
	 * @param numberOfThreads
	 *            number of sound threads
	 */
	public SoundManager(int numberOfThreads) {
		soundPool = Executors.newFixedThreadPool(numberOfThreads);
	}

	/**
	 * Load a sound from a file
	 *
	 * @param id
	 *            - String identifier of a sound
	 * 
	 * @param file
	 *            - path to the file
	 */
	public void load(String id, String file) {
		URL url = getClass().getResource(file);
		AudioClip sound = new AudioClip(url.toExternalForm());
		soundMap.put(id, sound);
	}

	/**
	 * Play a sound by id
	 *
	 * @param id
	 *            of a sound to be played.
	 */
	public void play(final String id) {
		Runnable soundPlay = new Runnable() {
			@Override
			public void run() {
				log.fine("Playing sound");
				soundMap.get(id).play();
				log.fine("Done playing sound");
			}
		};
		log.fine("Starting thread for playing sound");
		soundPool.execute(soundPlay);
		log.fine("Sound thread started");
	}

	/**
	 * Stop all sound threads
	 */
	public void shutdown() {
		soundPool.shutdown();
	}

}
