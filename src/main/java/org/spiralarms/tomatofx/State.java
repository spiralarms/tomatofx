package org.spiralarms.tomatofx;

/*
 * All states of the application
 */
public enum State {

	IDLE, WORK, PAUSE, REST

}
