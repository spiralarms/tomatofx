package org.spiralarms.tomatofx;

import javafx.scene.image.ImageView;

public interface IIconController {

	/*
	 * @return true if icons were loaded properly, false otherwise
	 */
	boolean isAlive();

	/*
	 * @return icon if available
	 */
	ImageView get(Icons icon);
	
}
