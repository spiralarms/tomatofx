package org.spiralarms.tomatofx;


public interface IController {
	
	// Controller controls View
	void setView(IView view);
	
	// Initialize the state and start a timer task
	void start();
	
	// Stop timer task
	void stop();

	// State accessors
	int getCount();
	State getState();

	// Button handlers
	void playPressed();
	void stopPressed();
	void leftPressed();
	void rightPressed();

}
