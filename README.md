# README #

TomatoFX is a simple timer for [The Pomodoro Technique](https://en.wikipedia.org/wiki/Pomodoro_Technique).

Copyright (c) 2015 SpiralArms

Native installers (x86_64): [SourceForge](https://sourceforge.net/projects/tomatofx)

Source files (MIT License): [BitBucket](https://bitbucket.org/spiralarms/tomatofx)

## Building packages from sources ##

Java JDK 8 and Maven are required for building native packages.
Please consult [javafx-maven-plugin](https://github.com/javafx-maven-plugin/javafx-maven-plugin) for additional details.

* runnable jar only

    `mvn clean jfx:jar`

* runnable jar and native installer

    `mvn clean jfx:native`

The resulting binary packages can be found in `target/jfx/app` and `target/jfx/native` folders.


## Installation ##

* Debian/Ubuntu

    `sudo dpkg -i tomatofx-<version>.deb`

* Redhat/Fedora

    `rpm -Uvh tomatofx-<version>-1.x86_64.rpm`


## Run ##

* Linux

    `/opt/TomatoFX/TomatoFX`

* Any platform with JRE 8

    `java -jar TomatoFX-<version>-jfx.jar`


## Contacts ##

SpiralArms <spiralarms.org@gmail.com>