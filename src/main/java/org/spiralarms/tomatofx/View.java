package org.spiralarms.tomatofx;

import java.net.URL;
import java.util.ResourceBundle;
//import java.util.logging.Logger;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

public class View implements IView, Initializable {

	// private Logger log = Logger.getLogger(getClass().getName());

	IController controller;
	IIconController icon;

	@FXML
	private Text txtStatus;
	@FXML
	private Text txtTime;
	@FXML
	private Button btnLeft;
	@FXML
	private Button btnRight;
	@FXML
	private Button btnPlay;
	@FXML
	private Button btnStop;

	/*
	 * Should be called before the first update()
	 */
	public void setController(IController controller) {
		this.controller = controller;
	}

	/*
	 * No calls to controller here
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		icon = Main.getIconController();

		btnLeft.setGraphic(icon.get(Icons.LEFT));
		btnRight.setGraphic(icon.get(Icons.RIGHT));
		btnPlay.setGraphic(icon.get(Icons.START));
		btnStop.setGraphic(icon.get(Icons.BREAK));
	}

	/*
	 * Should be called after setController()
	 */
	@Override
	public void update() {
		int count = controller.getCount();
		int minutes = count / 60;
		int seconds = count % 60;
		String time = String.format("%02d:%02d", minutes, seconds);
		txtTime.setText(time);

		// I'm not very happy about all this dispatch
		// Logic in the View!
		// View decides what to draw for every state
		// Is it OK?

		switch (controller.getState()) {
		case WORK:
			// Time change is not allowed!
			btnLeft.setDisable(true);
			btnRight.setDisable(true);
			btnPlay.setDisable(false);
			// TODO: property, corresponding to the state of the button?
			if (btnPlay.getGraphic() != icon.get(Icons.PAUSE)) {
				btnPlay.setGraphic(icon.get(Icons.PAUSE));
			}
			btnStop.setDisable(false);
			txtStatus.setFill(Color.DODGERBLUE);
			txtStatus.setText("Work");
			break;
		case PAUSE:
			// Allow time change
			btnLeft.setDisable(count > 0 ? false : true);
			btnRight.setDisable(count < Controller.MAX_DURATION ? false : true);
			btnPlay.setDisable(false);
			if (btnPlay.getGraphic() != icon.get(Icons.START)) {
				btnPlay.setGraphic(icon.get(Icons.START));
			}
			btnStop.setDisable(false);
			txtStatus.setFill(Color.CORAL);
			txtStatus.setText("Pause");
			break;
		case IDLE:
			btnStop.setDisable(true);
			// Allow time change
			btnLeft.setDisable(count > 0 ? false : true);
			btnRight.setDisable(count < Controller.MAX_DURATION ? false : true);
			btnPlay.setDisable(false);
			if (btnPlay.getGraphic() != icon.get(Icons.START)) {
				btnPlay.setGraphic(icon.get(Icons.START));
			}
			txtStatus.setFill(Color.GRAY);
			txtStatus.setText("Idle");
			break;
		case REST:
			// Time change is not allowed!
			btnLeft.setDisable(true);
			btnRight.setDisable(true);
			txtStatus.setFill(Color.GREEN);
			txtStatus.setText("Rest");
			btnPlay.setDisable(false);
			if (btnPlay.getGraphic() != icon.get(Icons.START)) {
				btnPlay.setGraphic(icon.get(Icons.START));
			}
			btnStop.setDisable(false);
			break;
		}

	}

	
	// redirect button events to controller
	
	@FXML
	protected void handlePlayButtonAction(ActionEvent event) {
		controller.playPressed();
	}

	@FXML
	protected void handleStopButtonAction(ActionEvent event) {
		controller.stopPressed();
	}

	@FXML
	protected void handleLeftButtonAction(ActionEvent event) {
		controller.leftPressed();
	}

	@FXML
	protected void handleRightButtonAction(ActionEvent event) {
		controller.rightPressed();
	}

}
