package org.spiralarms.tomatofx;

import java.io.IOException;
import java.util.Timer;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Parent;
import javafx.scene.Scene;

public class Main extends Application {

	private Logger log = Logger.getLogger(getClass().getName());

	private static Timer timer;
	private static ISoundController soundController;
	private static IIconController iconController;
	private static IController controller;
	private static IView view;


	public static Timer getTimer() {
		return timer;
	}


	public static ISoundController getSoundController() {
		return soundController;
	}


	public static IIconController getIconController() {
		return iconController;
	}


	/*
	 * Launcher thread. Do not build stage or scene here. Construction of other
	 * JavaFX objects is allowed
	 */
	public void init() {
		
		// Set up logger configuration
		try {
			LogManager.getLogManager().readConfiguration(Main.class.getResourceAsStream("/logging.properties"));
		} catch (SecurityException | IOException e) {
			e.printStackTrace();
		}
		
		log.fine("Hello!");
		log.fine("Creation of non-GUI objects.");

		timer = new Timer(true);
		log.fine("Timer created.");

		soundController = new SoundController();
		iconController = new IconController();

		// Controller should be created before View!
		controller = new Controller();
		log.fine("Controller created.");

		log.fine("All non-GUI objects created.");
	}

	
	/*
	 * FX thread.
	 */
	@Override
	public void start(Stage primaryStage) {
		try {
			log.fine("Creation of GUI objects");

			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/SimpleView.fxml"));
			Parent root = (Parent) loader.load();
			log.fine("Root node loaded.");

			log.fine("Trying to get a reference to View.");
			view = loader.<View> getController();
			if (view != null) {
				log.fine("The reference to View is OK.");
				// dependencies
				view.setController(controller);
				controller.setView(view);
			} else {
				log.severe("The reference to View is null.");
			}

			Scene scene = new Scene(root, 590, 384);
			primaryStage.setScene(scene);
			primaryStage.setTitle("TomatoFX 0.2");
			primaryStage.show();
			log.fine("Main window created.");

			controller.start();
			log.fine("Controller started");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	/*
	 * Launcher thread. Exit application via Platform.exit, then this method
	 * will be called!
	 */
	public void stop() {
		log.fine("Shutdown of the application");
		controller.stop();
		timer.cancel();
		soundController.stop();
		log.fine("Good-bye!");
	}

	
	public static void main(String[] args) {
		launch(args);
	}
}
