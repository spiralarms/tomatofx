package org.spiralarms.tomatofx;

import java.util.*;
import java.util.logging.Logger;

import javafx.application.Platform;

public class Controller implements IController {

	// Constants related to the parameters counting task
	
//	private static final int INITIAL_REST_DURATION = 10;
//	private static final int INITIAL_WORK_DURATION = 10;
//	private static final int DELTA_DURATION = 5;
//	public static final int MAX_DURATION = 60;

	private static final int INITIAL_WORK_DURATION = 25 * 60;
	private static final int INITIAL_REST_DURATION = 5 * 60;
	private static final int DELTA_DURATION = 5 * 60;
	public static final int MAX_DURATION = 60 * 60;

	private Logger log = Logger.getLogger(getClass().getName());

	// State variables
	private int count;
	private State state;

	// External references to application objects
	private Timer timer;
	ISoundController snd;
	IView view;

	// The counting task
	private TimerTask timerTask;

	/*
	 * Should be called before start()
	 */
	@Override
	public void setView(IView view) {
		this.view = view;
	}

	/*
	 * FX thread. Should be called after setView()
	 */
	public void start() {
		// get dependencies
		this.timer = Main.getTimer();
		this.snd = Main.getSoundController();

		// set initial state
		count = INITIAL_WORK_DURATION;
		state = State.IDLE;

		// start timer task
		timerTask = new TimerTask() {
			@Override
			public void run() {
				tictoc();
			}
		};
		
		// one call per second
		timer.scheduleAtFixedRate(timerTask, 0, 1000);
		log.fine("Timer task started");

		// now it is possible to update GUI from the controller
		view.update();
	}

	// accessors for View

	public State getState() {
		return state;
	}

	public int getCount() {
		return count;
	}

	/*
	 * Timer thread
	 */
	public void tictoc() {
		if (state == State.WORK || state == State.REST) {
			count -= 1;
			if (count < 0) {
				if (state == State.WORK) {
					snd.play(Sounds.END_WORK);
					count = INITIAL_REST_DURATION;
					state = State.REST;
				} else
				if (state == State.REST) {
					snd.play(Sounds.END_REST);	
					count = INITIAL_WORK_DURATION;
					state = State.IDLE;
				}
			}
			Platform.runLater(() -> view.update());
		}
	}

	/*
	 * FX thread
	 */
	@Override
	public void playPressed() {
		switch (state) {
		case IDLE:
			snd.play(Sounds.BEGIN_WORK);
			state = State.WORK;
			break;
		case REST:
			snd.play(Sounds.BEGIN_WORK);
			state = State.WORK;
			count = INITIAL_WORK_DURATION;
			break;
		case PAUSE:
			snd.play(Sounds.RESUME_WORK);
			state = State.WORK;
			break;
		case WORK:
			snd.play(Sounds.PAUSE_WORK);
			state = State.PAUSE;
			break;
		}
		view.update();
	}

	/*
	 * FX thread
	 */
	@Override
	public void stopPressed() {
		if (state == State.WORK) snd.play(Sounds.BREAK_WORK);
		if (state == State.REST) snd.play(Sounds.BREAK_REST);
		state = State.IDLE;
		count = INITIAL_WORK_DURATION;
		view.update();
	}

	/*
	 * FX thread
	 */
	@Override
	public void leftPressed() {
		if (count >= DELTA_DURATION) {
			count -= DELTA_DURATION;
		} else {
			count = 0;
		}
		view.update();
		snd.play(Sounds.CLICK);
	}

	/*
	 * FX thread
	 */
	@Override
	public void rightPressed() {
		if (count <= MAX_DURATION - DELTA_DURATION) {
			count += DELTA_DURATION;
		} else {
			count = MAX_DURATION;
		}
		view.update();
		snd.play(Sounds.CLICK);
	}

	/*
	 * Launcher thread
	 */
	@Override
	public void stop() {
		if (timerTask != null) {
			timerTask.cancel();
		}
	}

}
