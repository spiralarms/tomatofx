package org.spiralarms.tomatofx;


/*
 * All sounds of the application
 */
public enum Sounds {

	BEGIN_WORK, END_WORK, PAUSE_WORK, RESUME_WORK, BREAK_WORK, END_REST, BREAK_REST, CLICK

}
