package org.spiralarms.tomatofx;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class IconManager {

	private Map<String, ImageView> imageMap = new HashMap<>();

	
	public void load(String id, String file) {
		InputStream stream = getClass().getResourceAsStream(file);
		Image image = new Image(stream);
		ImageView imageView = new ImageView(image);
		imageMap.put(id, imageView);
	}

	
	public ImageView get(final String id) {
		return imageMap.get(id);
	}

}
