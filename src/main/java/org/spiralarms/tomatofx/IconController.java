package org.spiralarms.tomatofx;

import java.util.logging.Logger;

import javafx.scene.image.ImageView;

public class IconController implements IIconController {

	private static final String PATH = "/icons/";

	private final String[][] names = { { Icons.LEFT.name(), "metal_left.png" },
			{ Icons.RIGHT.name(), "metal_right.png" }, { Icons.START.name(), "metal_play.png" },
			{ Icons.PAUSE.name(), "metal_pause.png" }, { Icons.BREAK.name(), "metal_stop.png" } };

	private Logger log = Logger.getLogger(getClass().getName());

	private IconManager iconManager;

	/*
	 * Internal activity state False value means "icons are not available"
	 */
	private boolean alive = false;

	/*
	 * Try to load icons If successful, go to active state
	 */
	public IconController() {
		loadIcons();
	}

	private void loadIcons() {
		try {
			iconManager = new IconManager();
			for (String[] name : names) {
				iconManager.load(name[0], PATH + name[1]);
			}
			alive = true;
			log.fine("Success.");
		} catch (Exception e) {
			log.warning("Failure.");
		}
	}

	@Override
	public boolean isAlive() {
		return alive;
	}

	@Override
	public ImageView get(Icons icon) {
		return iconManager.get(icon.name());
	}

}
