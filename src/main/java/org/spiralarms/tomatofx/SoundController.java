package org.spiralarms.tomatofx;

import java.util.logging.Logger;
import static org.spiralarms.tomatofx.Sounds.*;

/*
 * Owns SoundManager
 */
public class SoundController implements ISoundController {

	private static final String PATH = "/sounds/";
	private static final String EXT = ".wav";

	private final String[][] names = {
			{ BEGIN_WORK.name(),  "gong"+EXT },
			{ PAUSE_WORK.name(),  "pause"+EXT },
			{ RESUME_WORK.name(), "clink"+EXT },
			{ BREAK_WORK.name(),  "cancel"+EXT },
			{ END_WORK.name(),    "applause"+EXT },
			{ CLICK.name(),       "tick"+EXT },
			{ END_REST.name(),    "clink"+EXT },
			{ BREAK_REST.name(),  "cancel"+EXT }
	};

	private Logger log = Logger.getLogger(getClass().getName());

	private SoundManager soundManager;

	/*
	 * Internal activity state False value means "no sound"
	 */
	private boolean alive = false;

	/*
	 * Try to initialize sound manager If successful, go to active state
	 */
	public SoundController() {

		try {
			log.fine("Trying to create Sound Manager");
			soundManager = new SoundManager(2);
			log.fine("Success");

			log.fine("Trying to load sounds");
			for (String[] name : names) {
				soundManager.load(name[0], PATH + name[1]);
			}
			alive = true;
			log.fine("Sound Manager initialized successfully.");
		} catch (Exception e) {
			log.severe("Problem with Sound Manager.");
		}
	}

	@Override
	public boolean isAlive() {
		return alive;
	}

	@Override
	public void play(Sounds sound) {
		if (alive) {
			soundManager.play(sound.name());
		}
	}

	public void stop() {
		if (alive) {
			soundManager.shutdown();
		}
	}

}
