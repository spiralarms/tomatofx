package org.spiralarms.tomatofx;

/*
 * Common interface to sound system
 */
public interface ISoundController {
	
	/*
	 * @return true if sound system was initialized properly, false otherwise
	 */
	boolean isAlive();
	
	/*
	 * Play a sound in a thread, return immediately
	 */
	void play(Sounds sound);
	
	/*
	 * Shutdown the sound system
	 */
	void stop();
	
}
