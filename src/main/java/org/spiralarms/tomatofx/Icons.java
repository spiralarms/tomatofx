package org.spiralarms.tomatofx;


/*
 * All icons of the application
 */
public enum Icons {
	
	LEFT, RIGHT, START, PAUSE, BREAK

}
